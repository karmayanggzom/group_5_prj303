# Handwritten Dzongkha Alphabet Recognition System using Deep Learning

*Gyalpozhing College of Information 
Technology(Bachelor of Science in Information 
Technology(BScIT)) Year 3 Project*

# Description

There are many research and applications of handwritten OCR in various languages such as the Devanagari text of India and the Farsi text of Arab, there has been no research conducted towards the Dzongkha alphabet of Bhutan.  To address this issue and to promote the Dzongkha language, computerization of the language is a must and OCR is one such solution to overcome this obstacle. Handwritten Dzongkha Alphabets Recognition System (གསལ་བྱེད) is an application built for our Third Year Project. In additional too that we have also come us with a simple website which provide us all the details of our system and guide. Handwritten Dzongkha Alphabets Recognition System website also allow you to make prediction similar to the app.

# Technology Used
1. Google colab/Jupyter notebook
2. Tensorflow
3. Visual Studio Code
4. Python
5. Django 
6. OpenCV
7. Heroku

Visit us on :  http://handwritten-dzongkha.herokuapp.com

## Video
Watch video here : 
(https://youtu.be/1aiiCUmvyjQ)

## Screenshoot of Website
<img src="Screenshoot/1.png" width="400" height="250" /> 
<img src="Screenshoot/2.png" width="400" height="250" />
<img src="Screenshoot/3.png" width="400" height="250" />
<img src="Screenshoot/4.png" width="400" height="250" />
<img src="Screenshoot/5.png" width="400" height="250" />

# Poster
<img src="Screenshoot/Poster.jpeg" width="250" height="400" />
