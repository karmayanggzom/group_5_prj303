import os
import cv2 
import numpy as np
import tensorflow as tf
from Handwritten_Dzongkha.settings import BASE_DIR
import json

def alphabet_prediction(img):
    # Importing the trained model file
    model = tf.keras.models.load_model(os.path.join(BASE_DIR, "home/model/Model_Handwriten.h5"))

    # Performing some transformations on the input image numpy array to provide better prediction
    img_gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    #gray = to_grayscale(img)
    img_inverted = cv2.bitwise_not(img_gray)
    img_resized = cv2.resize(img_inverted, (150, 150))
    img_reshaped = img_resized.reshape((1, 150, 150))
    # Predicting the number present in the input image
    prediction = np.argmax(model.predict(img_reshaped))
    return prediction

