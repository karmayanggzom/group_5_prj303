from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from .model_prediction import alphabet_prediction
import numpy as np
import cv2 
import json 


json_data =""" {
    "0": "Unknown",
    "1": "ཀ",
    "2": "ཁ",
    "3": "ག",
    "4": "ང",
    "5": "ཅ",
    "6": "ཆ",
    "7": "ཇ",
    "8": "ཉ",
    "9": "ཏ",
    "10": "ཐ",
    "11": "ད",
    "12": "ན",
    "13": "པ",
    "14": "ཕ",
    "15": "བ",
    "16": "མ",
    "17": "ཙ",
    "18": "ཚ",
    "19": "ཛ",
    "20": "ཝ",
    "21": "ཞ",
    "22": "ཟ",
    "23": "འ",
    "24": "ཡ",
    "25": "ར",
    "26": "ལ",
    "27": "ཤ",
    "28": "ས",
    "29": "ཧ",
    "30": "ཨ"
}"""
data = json.loads(json_data)

# Create your views here.
def home(request, *args, **kwargs):
    if request.method == "GET":
        context = {
            "title": "Handwritten Dzongkha Alphabets Recognition System"
        }
        return render(request, 'home/Index.html') 

    elif request.method == "POST":
        image = request.FILES.get('numImg')
        img = cv2.imdecode(np.fromstring(image.read(), np.uint8), 1)

        #labelInfo[str(np.argmax(img))]
        prediction = alphabet_prediction(img)
        prediction = data[str(prediction)]
        print (prediction)
        context = {
            "prediction": prediction
        }
        return render(request, 'home/success.html', context)
    
    
  